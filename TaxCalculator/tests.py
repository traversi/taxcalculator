"""
Author: Marcello Traversi
E-mail: me@marcellotraversi.com
Year: 2014
"""
from django.test import TestCase

class TaxCalculatorTest(TestCase):
	def setUp(self):
		"""
		self.currency = 'GBP'
		self.product_code = 'WINE123'
		self.price = '15'
		"""
		self.input_list =  [
				{'currency': 'GBP', 'product_code': 'WINE123', 'price': '12.00'},
				{'currency': 'GBP', 'product_code': 'ABC123', 'price': '2.50'},
				{'currency': 'EUR', 'product_code': 'CIGA', 'price': '4.80'},
				{'currency': 'GBP', 'product_code': 'CIGA', 'price': 'asd'},
				{'currency': 'USD', 'product_code': 'CAKE', 'price': '15.00'},
				{'currency': 'EUR', 'product_code': 'CAKE', 'price': '15.00'},
				{'currency': 'ASD', 'product_code': 'NOT_EXISTS', 'price': '10.00'}
		]

	def test_taxCalculator_GET(self):
		for pars in self.input_list:
			response = self.client.get('/currency/%s/pcode/%s/price/%s/' % (pars['currency'], pars['product_code'], pars['price']))
			#print(response.content)
		self.assertEqual(response.status_code, 200)
"""
Author: Marcello Traversi
E-mail: me@marcellotraversi.com
Year: 2014
"""
from django.conf.urls import patterns, url

from .views import taxCalculator

urlpatterns = patterns('',
	url(r'^currency/(?P<currency>\w{3,4})/pcode/(?P<product_code>\w{3,10})/price/(?P<price>[.\w]+)/$', taxCalculator),
)

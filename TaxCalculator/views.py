"""
Author: Marcello Traversi
E-mail: me@marcellotraversi.com
Year: 2014
"""
from django.shortcuts import render
from django.http import HttpResponse

from .taxCalculator import TaxCalculator

def taxCalculator(self, currency, product_code, price):
	calculator = TaxCalculator()
	output = calculator.calculate_tax(currency, product_code, price)
	return HttpResponse(output, mimetype='text/html')
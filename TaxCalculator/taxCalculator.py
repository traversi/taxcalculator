#!/usr/bin/python
#coding:utf-8
"""
Author: Marcello Traversi
E-mail: me@marcellotraversi.com
Year: 2014
"""

import sys

class TaxCalculator():
	currencies = {'EUR': {'country': 'Germany', 'symbol': '&euro;'},
				'GBP': {'country': 'United Kingdom', 'symbol': '&pound;'},
				'USD': {'country': 'United States of America', 'symbol': '&#36;'}}

	products = {'ABC123': {'description': 'Bread'},
				'WINE123': {'description': '75cl bottle of wine'},
				'CIGA': {'description': 'Cigarettes'},
				'CAKE': {'description': 'Cake'}}

	taxes = {'ABC123': {'GBP': {'tax': '20', 'tax_type': 'Percentage'}},
			'WINE123': {'GBP': {'tax': '2', 'tax_type': 'Fixed'}},
			'CIGA': {'GBP': {'tax': '25', 'tax_type': 'Percentage'},
					'EUR': {'tax': '20', 'tax_type': 'Percentage'}}}

	def calculate_tax(self, currency, product_code, price):
		try:
			float(price)
		except:
			return("The price is not valid.\n")

		currency = currency.upper()
		product_code = product_code.upper()
		
		if self.currencies.has_key(currency):	
			if self.products.has_key(product_code):
				description = self.products[product_code]['description']
				symbol = self.currencies[currency]['symbol']
				country = self.currencies[currency]['country']
			
				if country == 'Germany':
					taxable_amount = float(price) - 2.0
				else:
					taxable_amount = float(price)
				
				if self.taxes.has_key(product_code):
					tax = self.taxes[product_code][currency]['tax']
					tax_type = self.taxes[product_code][currency]['tax_type']
			
					if tax_type == 'Percentage':
						tax_amount = float(taxable_amount) * float(tax)/100
			
						return("VAT on %s worth %s%s in %s is %s%s (%s%%)\n" % (description, symbol, price, country, symbol, tax_amount, tax))

					elif tax_type == 'Fixed':
						tax_amount = float(taxable_amount) - float(tax)
				
						return("Tax on %s worth %s%s is %s%s\n" % (description, symbol, price, symbol, tax))	
				else:	
					tax_amount = float(taxable_amount) * 0.1
			
					return("VAT on %s worth %s%s in %s is %s%s (10%%)\n" % (description, symbol, price, country, symbol, tax_amount))
			else:	
				return("Product %s doesn't exists.\n" % product_code)
		else:
			return("Currency %s doesn't exists.\n" % currency)

if __name__ == '__main__':
	first_inputs = [{'currency': 'GBP', 'product_code': 'WINE123', 'price': '12.00'},
					{'currency': 'GBP', 'product_code': 'ABC123', 'price': '2.50'},
					{'currency': 'EUR', 'product_code': 'CIGA', 'price': '4.80'},
					{'currency': 'GBP', 'product_code': 'CIGA', 'price': 'asd'},
					{'currency': 'USD', 'product_code': 'CAKE', 'price': '15.00'},
					{'currency': 'EUR', 'product_code': 'CAKE', 'price': '15.00'},
					{'currency': 'ASD', 'product_code': 'NOT_EXISTS', 'price': '10.00'}]
				
	calculator = TaxCalculator()

	for v in first_inputs:
		calculator.calculate_tax(v['currency'], v['product_code'], v['price'])
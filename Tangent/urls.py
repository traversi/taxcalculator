"""
Author: Marcello Traversi
E-mail: me@marcellotraversi.com
Year: 2014
"""
from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
	url(r'', include('TaxCalculator.urls')),
    #url(r'^admin/', include(admin.site.urls)),
)
